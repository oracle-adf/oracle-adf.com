# [oracle-adf.com](https://oracle-adf.com) source codes

<br/>

### Run oracle-adf.com on localhost

    # vi /etc/systemd/system/oracle-adf.com.service

Insert code from oracle-adf.com.service

    # systemctl enable oracle-adf.com.service
    # systemctl start oracle-adf.com.service
    # systemctl status oracle-adf.com.service

http://localhost:4031
