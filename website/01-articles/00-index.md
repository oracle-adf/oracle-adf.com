---
layout: page
title: Info
permalink: /articles/
---

<div class="row-fluid">
  <div class="col-md-12">

    <h3>SITEMAP</h3>

    <div class="row-fluid">
      <div class="col-md-8">

          <a href="https://www.youtube.com/watch?v=3my92HTBTP4">Jdeveloper & Git Repository</a> (YouTube)<br/>

          <br/><br/>

          <strong>Tips:</strong><br/><br/>


          <a href="/learn-oracle-adf/tips/adf-logger/">ADF Logger</a> <br/>

          <a href="http://www.awasthiashish.com/2015/09/adf-basics-call-pl-sql-stored-function-in-adf-application.html" rel="nofollow">ADF Basics: Call PL/SQL Stored function in ADF Application</a><br/>

          <a href="/learn-oracle-adf/tips/how-to-execute-a-method-before-the-jsf-page-gets-loaded/">How to execute a method before the jsf page gets loaded</a><br/>

          <a href="/learn-oracle-adf/tips/adf-model-generating-and-using-in-clause-in-vo-sql-statement/">ADF Model: Generating and using 'in' clause in VO sql statement</a><br/>

          <a href="/learn-oracle-adf/tips/ADFUtil-class-to-evaluate-set-and-invoke-EL-expressions/">ADF UI - ADFUtil class to evaluate, set and invoke EL expressions</a><br/>

          <a href="/learn-oracle-adf/tips/Getting-all-pagedef-attributes-of-a-particular-VO-iterator-programatically/">ADF UI - Getting all pagedef attributes of a particular VO iterator programatically</a><br/>

          <a href="/learn-oracle-adf/tips/Programmatically-executing-view-criteria-in-AMImpl-method/">ADF Model: Programmatically executing view criteria in AMImpl method</a><br/>

           <br/><br/>

          <strong>Migrating:</strong><br/><br/>

          <a href="/learn-oracle-adf/migrating/migrating-from-oracle-forms-to-oracle-adf/">Migrating from Oracle Forms to Oracle ADF</a> <br/>

          <br/><br/>

          <strong>Deploying ADF application in Oracle Cloud:</strong><br/><br/>

          <a href="http://waslleysouza.com.br/en/2015/03/deploying-adf-application-to-oracle-cloud/" rel="nofollow">article</a> app <a href="https://bitbucket.org/oracle-adf/adfcloudapp/src/master/">src</a><br/>


          <br/><br/>

          <strong>ViewCriteria</strong>


            <ul>
                <li><a href="https://www.youtube.com/watch?v=04ZpxEn-xeA">Programmatically access a view criteria</a> (YouTube)</li>
            </ul>



          <br/><br/>

          <h3>My Samples or Samples what I tried</h3>
          <a href="/my/implementing-search-for-multiple-attributes-of-a-view-object/">Implementing Search for Multiple Attributes of a View Object</a> (Very Helpful Example) <br/>


          <br/><br/>

          <h3>Info:</h3>

          <a href="/docs/info/scopes-in-fusion-web-applications/">Scopes in Fusion Web Applications</a><br/>

          <br/><br/>

          <h3>Materials what I should try:</h3>

          <a href="https://technology.amis.nl/2011/12/01/adf-11g-close-popup-with-taskflow-inside-or-complete-taskflow-inside-popup/">ADF 11g – close popup with taskflow inside or complete taskflow inside popup</a><br/>

            <a href="http://jdeveloperandadf.blogspot.ru/2011/03/execute-code-when-enter-or-exit-any.html">ADF Page Template Refresh From ADF Page Fragment</a><br/>

          <a href="http://andrejusb.blogspot.com.es/2012/02/adf-page-template-refresh-from-adf-page.html">ADF Page Template Refresh From ADF Page Fragment</a> <a href="https://bitbucket.org/oracle-adf/templaterefreshapp/src/master/">src</a><br/>

          <a href="http://andrejusb.blogspot.ru/2012/07/refreshing-adf-page-template-from-adf.html">Refreshing ADF Page Template from ADF Fragment Template</a> <a href="https://bitbucket.org/oracle-adf/templaterefreshapp_v2/src/master/">src</a><br/>

         <a href="https://docs.oracle.com/cd/A97335_02/apps.102/bc4j/developing_bc_projects/bc_pcreatingandchangingvoqueriesincode.htm">Creating and Changing View Object Queries in Code</a><br/>

         <a href="http://www.baigzeeshan.com/2011/05/how-to-run-java-code-on-every-page-load.html">How to run Java code on every page load in Oracle ADF</a> <a href="https://bitbucket.org/oracle-adf/runcodeoneverypage/src/master/">src</a><br/>

         <a href="http://www.awasthiashish.com/2015/09/using-popupfetchlistener-to-execute.html">Using popupFetchListener to execute method on popup launch in Oracle ADF</a><br/>

         <a href="http://fortunefusionminds.blogspot.ru/2012/10/how-to-refresh-ui-component-from.html">How to Refresh an UI Component from Backing bean in ADF</a><br/>


         <br/><br/>

         <h3>Materials what I should look:</h3>

        <a href="https://community.oracle.com/thread/3732821">ADF Refresh page jsf with page fragments jsff after set session variable</a><br/>

          <br/><br/>

          <strong>Some Code Samples:</strong><br/><br/>

          <a href="/apps/sample-oracle-adf-application/">Sample Oracle ADF Application</a> <br/>

          <a href="http://www.oracle.com/technetwork/developer-tools/jdev/index-098948.html" rel="nofollow">ADF Summit sample open source application</a> <br/>

          <a href="https://bitbucket.org/oracle-adf/adfaltaapp_v12/src/master/" rel="nofollow">ADFAltaApp_v12</a><br/>

          <a href="https://bitbucket.org/oracle-adf/mainpagewithpopup/src/master/" rel="nofollow">MainPageWithPopup Example</a> <br/>

          <a href="http://liuwuhua.blogspot.ru/2010/11/using-task-flow-as-popup-dialog.html" rel="nofollow">Using Task Flow as a Popup Dialog</a>



          <br/><br/>

          <strong>Groovy and Oracle ADF:</strong><br/><br/>

          <a href="https://groovyconsole.appspot.com/" rel="nofollow">Groovy Web Console</a> <br/>
          <a href="http://andrejusb.blogspot.ru/2013/02/adf-bc-groovy-with-java-imports.html" rel="nofollow">ADF BC Groovy with Java Imports</a> <br/>
          <a href="/files/pdf/groovy/introduction-to-groovy-128837.pdf" rel="nofollow">Introduction to Groovy Support in JDeveloper and Oracle ADF 11g</a>(PDF)<br/>


          <br/><br/>
          <br/><br/>

          <h3>If you have source codes with good code examples, please share it with us.</h3>

      </div>

      <div class="col-md-4">

          <a class="twitter-timeline" href="https://twitter.com/hashtag/oracleadf" data-widget-id="644478168791838721">#oracleadf Tweets</a>
          <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

      </div>
    </div>

  </div>
</div>

<!--
          Oracle JET

          <br/><br/>
          <br/><br/>

          http://www.oracle.com/webfolder/ux/middleware/alta/gallery/gallery.html<br/>
          http://www.oracle.com/webfolder/ux/middleware/alta/examples.html<br/>
          http://www.oracle.com/webfolder/technetwork/jet/uiComponents-formControls.html

-->
