---
layout: page
title: Set and Unset Where Clause in the Bean with iterator
permalink: /my/where-clause-in-bean-with-iterator/
---

# Set the Where Clause of a View Object

<div align = "center">

    <iframe width="853" height="480" src="https://www.youtube.com/embed/C2UCBI7oQ70" frameborder="0" allowfullscreen></iframe>

</div>

<br/><br/>

I interest only sample with bean

<br/>

# Example 1

ADFUtils you can get from this repo <a href="https://bitbucket.org/oracle-adf/adf-utils/src/master/">Utils</a>

    ApplicationModule am = ADFUtils.getApplicationModuleForDataControl("AppModuleDataControl");
    ViewObject vo = am.findViewObject("myViewObject");

    String WhereClauseParams = "";
    WhereClauseParams = "TRUNC(DATE_CREATION) = TRUNC(SYSDATE)";

    vo.setWhereClause(WhereClauseParams);
    vo.executeQuery();

<br/>

# Example 2

<br/><br/>

    import oracle.adf.model.binding.DCBindingContainer;
    import oracle.adf.model.binding.DCIteratorBinding;

    ****

    DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
    DCIteratorBinding iter = bindings.findIteratorBinding("Sups_ListIterator");

    iter.getViewObject().setWhereClause("JOB_ID=:TheJOB_ID");
    iter.getViewObject().defineNamedWhereClauseParam("TheJOB_ID", null, null);
    iter.getViewObject().setNamedWhereClauseParam("TheJOB_ID", "IT_PROG");
    iter.executeQuery();

<br/>

### To unsetWhereClause

    private void clearWhereClauseParams(String iteratorName){

        DCBindingContainer bindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = bindings.findIteratorBinding(iteratorName);

        AttributeList attributeList =  iter.getViewObject().getNamedWhereClauseParams();
        String attrNames[] = attributeList.getAttributeNames();

        for(int j=0;j<attrNames.length;j++){
            iter.getViewObject().removeNamedWhereClauseParam(attrNames[j]);
        }

        iter.getViewObject().setWhereClause(null);
    }

<br/>

### See also

    AttributeList attributeList = myViewObject.getNamedWhereClauseParams();

    // from attributeList get attributeNames - which are your bind variable names

    String attrNames[] = attributeList.getAttributeNames();
    for(int j=0;j<attrNames.length;j++)
    myViewObject.removeNamedWhereClauseParam(attrNames[j]);

    //finally clear where clause

    myViewObject.setWhereClause(null);

https://community.oracle.com/thread/892886
