---
layout: page
title:  Execute With Params in Bean
permalink: /my/execute-with-params-in-bean/
---

# Execute With Params in Bean


I have viewObject with parameter. I want to execute this ViewObject in Bean.
Something like this in the end of VO.

    Where 1=1
        and supplier = nvl(:b_supplier, supplier)  
        and UPPER (SUP_NAME) LIKE UPPER (:p_sups_search_by_name || '%')

<br/>

For do this, I created in binding section new action.<br/>
Then I chosen my iterator with execute with params option.<br/>
Setup parameter name and type.

After that I created bean with next logic it method:

<br/><br/>

    BindingContainer bindings = BindingContext.getCurrent().getCurrentBindingsEntry();
    OperationBinding ob = bindings.getOperationBinding("ExecuteWithParams4");
    ob.getParamsMap().put("p_sups_search_by_name", "parameter1");
    Object result1 = ob.execute();
