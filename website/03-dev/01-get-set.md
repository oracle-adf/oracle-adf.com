---
layout: page
title:  Oracle ADF -> Get/Set PageFlowScope values in bean
permalink: /my/get-set-pageflowscope-values-in-bean/
---

# Get/Set PageFlowScope values in bean

### Set PageFlowScope values

    ADFContext adfCtx = ADFContext.getCurrent();
    Map pageFlowScope = adfCtx.getPageFlowScope();
    Object val = pageFlowScope.put("name", "andrey");

<br/>

### GET PageFlowScope values

    ADFContext adfCtx = ADFContext.getCurrent();
    Map pageFlowScope = adfCtx.getPageFlowScope();
    Object val = pageFlowScope1.get("name");
    System.out.println(val.toString());

OR

    AdfFacesContext facesCtx= null;
    facesCtx= AdfFacesContext.getCurrentInstance();
    Map<String, Object> scopeVar= facesCtx.getPageFlowScope();
    for ( String key  : scopeVar.keySet() ) {
        System.out.println("key: "+key);
        System.out.println("value: "+scopeVar.get(key));
    }


More info:  
http://biemond.blogspot.ru/2011/01/get-pageflowscope-of-region-bounded.html

<br/><br/>

ADDITIONAL

    RequestContext.getCurrentInstance().getPageFlowScope().put("p_category", '5');

<br/>

    System.out.println(RequestContext.getCurrentInstance().getPageFlowScope().get("p_category"));


<!--

ADFContext.getCurrent().getPageFlowScope().get("kindComponent");


RequestContext.getCurrentInstance().getPageFlowScope().get("kindComponent");


AdfFacesContext.getCurrentInstance().getPageFlowScope().get("kindComponent");

-->


<br/><br/>


Scopes can be accessed from Expression Language and Java to perform read and write operations.
The following table shows an overview of scope access for the application scope, session scope,
request scope, pageFlow scope, backingBean scope, and view scope. The returned object is
always of Map<String, Object> type.


<br/>

\#{applicationScope}


    FacesContext fctx = FacesContext.getCurrentInstance();
    fctx.getExternalContext().getApplicationMap();
    or
    ADFContext adfCtx = ADFContext.getCurrent();
    adfCtx.getApplicationScope();

<br/>

\#{sessionScope}

    FacesContext fctx = null;
    fctx = FacesContext.getCurrentInstance();
    fctx.getExternalContext().getSessionMap();
    or
    ADFContext adfCtx = ADFContext.getCurrent();
    adfCtx.getSessionScope();

<br/>

\#{requestScope}

    FacesContext fctx = null;
    fctx = FacesContext.getCurrentInstance();
    fctx.getExternalContext().getRequestMap();
    or
    ADFContext adfCtx = ADFContext.getCurrent();
    adfCtx.getRequestScope();

<br/>

\#{pageFlowScope}

    AdfFacesContext adfFacesContext = null;
    adfFacesContext = AdfFacesContext.getCurrentInstance();
    Map _pageFlowScope = adfFacesContext.getPageFlowScope();

<br/>

\#{viewScope}

    AdfFacesContext adfFacesContext = null;
    adfFacesContext = AdfFacesContext.getCurrentInstance();
    Map _viewScope = adfctx.getViewScope();
    or
    ADFContext adfCtx = ADFContext.getCurrent();
    adfCtx.getViewScope()

<br/>

\#{backingBeanScope}

    AdfFacesContext adfFacesContext = null;
    adfFacesContext = AdfFacesContext.getCurrentInstance();
    BackingBeanScopeProviderImpl provider =
    adfFacesContext. getBackingBeanScopeProvider();
    Map backingBeanScope = null;
    backingBeanScope = provider.getCurrentScope();
