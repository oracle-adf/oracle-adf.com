---
layout: page
title: Oracle ADF -> How to receive selected value from SelectOneChoice
permalink: /dev/view/components/SelectOneChoice/get-value/
---


# [Oracle ADF] How to receive selected value from SelectOneChoice

<br/>

### Example 1 (100% works):

<br/>

{% highlight xml linenos %}

<af:selectOneChoice value="#{bindings.AttributeBindingsName.inputValue}"
                    autoSubmit="true"
                    label="#{bindings.AttributeBindingsName.label}"
                    required="#{bindings.AttributeBindingsName.hints.mandatory}"
                    shortDesc="#{bindings.AttributeBindingsName.hints.tooltip}" id="soc1"
                    valueChangeListener="#{MyBean.onSelect}">
    <f:selectItems value="#{bindings.AttributeBindingsName.items}" id="si1"/>
    <f:validator binding="#{bindings.AttributeBindingsName.validator}"/>
</af:selectOneChoice>

{% endhighlight %}


<br/>

**In bindings it is a list**

{% highlight java linenos %}

public void onSelect(ValueChangeEvent valueChangeEvent) {

    RichSelectOneChoice soc = (RichSelectOneChoice)valueChangeEvent.getComponent();

    System.out.println("Index: " + soc.getValue().toString());

    // Value

    // Set Iterator on selected index from SOC
    valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());

    Object value = ADFUtils.findIterator("ITERATOR_NAME").getCurrentRow().getAttribute("ATTRIBUTE_NAME");
    System.out.println("Value " + value.toString());
}

{% endhighlight %}

<br/>

Of course attribute should be defined in ViewObject.



<br/>

### Example 2 (100% works):

You can use it method when SelectOneChoice is receiving index of element. (Not value):


{% highlight java linenos %}

public void onSelect(ValueChangeEvent valueChangeEvent) {

    RichSelectOneChoice soc = (RichSelectOneChoice)valueChangeEvent.getComponent();

    // Value

    Object value = ADFUtils.findIterator("ITERATOR_NAME").getRowAtRangeIndex((Integer)soc.getValue()).getAttribute("ATTRIBUTE_NAME");
    System.out.println("Value " + value.toString());
}

{% endhighlight %}
