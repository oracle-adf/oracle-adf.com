---
layout: page
title: Oracle ADF -> SelectOneChoice и RichSelectOneChoice
permalink: /dev/view/components/SelectOneChoice/
---


# [Oracle ADF] SelectOneChoice и RichSelectOneChoice


<br/><br/>

**Sample:**

{% highlight xml linenos %}

<af:selectOneChoice label="Без Сортировки" id="soc1" styleClass="page-title"
                    mode="compact" simple="true"
                    value="0" autoSubmit="true"
                    valueChangeListener="#{TasksFilterBean.onTasksFilterDateChanged}">
  <af:selectItem label="Без Сортировки" value="0"
                 id="si0"/>
  <af:selectItem label="Текущий день" value="1"
                 id="si1"/>
  <af:selectItem label="Вчера" value="2"
                 id="si2"/>
  <af:selectItem label="Эта неделя" value="3"
                 id="si3"/>
  <af:selectItem label="Текущий месяц" value="4"
                 id="si4"/>
  <af:selectItem label="Текущий квартал" value="5"
                 id="si5"/>
</af:selectOneChoice>

{% endhighlight %}

<br/><br/>


{% highlight java linenos %}

public void onTasksFilterDateChanged (ValueChangeEvent valueChangeEvent) {

    System.out.println("===========================");
    System.out.println(valueChangeEvent.getNewValue());
    System.out.println("===========================");


// Or

    RichSelectOneChoice soc = (RichSelectOneChoice)valueChangeEvent.getComponent();
    String var1 = soc.getValue().toString();


}

{% endhighlight %}

<br/><br/>

SOC can return value or index.

{% highlight java linenos %}

    System.out.println("");
    System.out.println(valueChangeEvent.getNewValue().toString());
    System.out.println("");

{% endhighlight %}

To chose it, Bindings --> pageDef.

Parameter: SelectItemValueMode: {ListObject, LixtIndex}



<br/><br/>

<strong>Samples:</strong>

<ul>
    <li><a href="/dev/view/components/SelectOneChoice/get-value/">How to receive selected value from SelectOneChoice</a></li>
    <li><a href="/dev/view/components/SelectOneChoice/set-default-value/">How to setup default values for SelectOneChoice programmatically</a></li>
</ul>
