---
layout: page
title: Oracle ADF -> How to setup default values for SelectOneChoice programmatically
permalink: /dev/view/components/SelectOneChoice/set-default-value/
---


# [Oracle ADF] How to setup default values for SelectOneChoice programmatically


<br/><br/>

{% highlight xml linenos %}

<af:selectOneChoice value="#{pageFlowScope.Statistics.monthDefaultValue}" id="soc2" autoSubmit="true" ">
    <af:selectItem label="Январь" value="1" id="si1"/>
    <af:selectItem label="Февраль" value="2" id="si2"/>
    <af:selectItem label="Март" value="3" id="si3"/>
    <af:selectItem label="Апрель" value="4" id="si4"/>
    <af:selectItem label="Май" value="5" id="si5"/>
    <af:selectItem label="Июнь" value="6" id="si6"/>
    <af:selectItem label="Июль" value="7" id="si7"/>
    <af:selectItem label="Август" value="8" id="si8"/>
    <af:selectItem label="Сентябрь" value="9" id="si9"/>
    <af:selectItem label="Октябрь" value="10" id="si10"/>
    <af:selectItem label="Ноябрь" value="11" id="si11"/>
    <af:selectItem label="Декабрь" value="12" id="si12"/>
</af:selectOneChoice>

{% endhighlight %}

<br/>
<br/>

{% highlight java linenos %}

public class Statistics {

    // --------------------------------------

    private String monthDefaultValue;

    public void setMonthDefaultValue(String monthDefaultValue) {
        this.monthDefaultValue = monthDefaultValue;
    }

    public String getMonthDefaultValue() {
        return monthDefaultValue;
    }

    // ===============================================

    @PostConstruct
    public void init() {
       this.monthDefaultValue = "5";
    }

} // The End of Class;

{% endhighlight %}


<br/><br/>

That is only 1 method. It could be much more.
