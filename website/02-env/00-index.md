---
layout: page
title:  Environment for Development Oracle ADF Application
permalink: /env/
---

### Environment for Development Oracle ADF Application


<a href="https://javadev.org/sitemap/">JDK Installation</a><br/>

<a href="http://oracledba.net/database/installation/">Oracle DataBase Installation</a><br/>

<a href="https://javadev.org/docs/appserv/weblogic/12c/installation/">Oracle Weblogic Installation</a> (To work with ADF, JRF components should be installed)<br/>

<a href="http://www.oracle.com/technetwork/database/enterprise-edition/databaseappdev-vm-161299.html" rel="nofollow">Oracle Database Virtual Machine</a><br/>
