---
layout: page
title: Oracle ADF > Site Map
permalink: /sitemap/
---

<strong> Sorry, now I'm working much more on russian version of website</strong>

<br/>

# Articles:

<ul>
    <li><a href="/articles/"><strong>Articles</strong></a></li>
</ul>

<br/>

### Environment for Development:

<ul>
    <li><a href="/env/"><strong>Environment</strong></a></li>
</ul>

<br/>

### Development:

<ul>
    <li><a href="/dev/"><strong>Development</strong></a></li>
</ul>

<br/>

### OUR Libs for Development (You can add something by pull requests):

<ul>
    <li><a href="https://bitbucket.org/oracle-adf/adf-utils"><strong>ADF-UTILS</strong></a></li>
</ul>

<br/>

### Oracle ADF Exam (1Z0-554):

<ul>
    <li><a href="/files/pdf/exam/1z0-554-exam-study-guide.pdf" rel="nofollow">Oracle Application Development Framework 11g Essentials – Exam Guide</a></li>
    <li><a href="/files/pdf/exam/1Z0-554.pdf" rel="nofollow">Oracle 1z0-554</a></li>
</ul>

<br/>

### Oracle ADF Architecture Square:

<ul>
    <li><a href="http://www.oracle.com/technetwork/developer-tools/adf/learnmore/adfarchitect-1639592.html" rel="nofollow"><strong>Oracle ADF Architecture Square</strong></a></li>
</ul>

<!--

<br/>

### Testing:


<ul>
    <li><a href="/test/"><strong>Тестирование</strong></a></li>
</ul>

-->
