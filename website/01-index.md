---
layout: page
title: Oracle ADF
permalink: /
---

<div class="row-fluid">
  <div class="col-md-12">

    <h1>Oracle ADF Development with Oracle Jet!</h1>

    <br/>

    <div class="row-fluid">
      <div class="col-md-8">

      The website administrator want to find interesting examples on Oracle ADF and share it on this website for all.

<br/><br/>

This website use Jekyll technologies. Than mean what all source codes are stored in repository on github. You could download and run source codes on you local mashine. (If it would be interesting to someone, i will write instruction.)

<br/><br/>

I hope you will agree to share interesting materials with us or may be you will find something helpful for you.

<br/><br/>

<div align="center">
    <img src="/img/oracle-adf-development-process.jpg" alt="Oracle ADF Developer">
</div>

<br/><br/>

Actually for comfortable development needs 2 monitor. 1 for Jdeveloper and 1 for Web Browser || PL/SQL Developer || Some Instructions etc.

<br/>
<br/>

For contacts:<br/>

<img src="/img/a3333333mail.gif" alt="Marley">

<br/>

    </div>
      <div class="col-md-4">

        <a class="twitter-timeline" href="https://twitter.com/oracle_adf" data-widget-id="644485271514288128">Tweets by @oracle_adf</a>
        <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

      </div>
    </div>

  </div>
</div>
