---
layout: page
title: Implementing Search for Multiple Attributes of a View Object
permalink: /my/implementing-search-for-multiple-attributes-of-a-view-object/
---

# Implementing Search for Multiple Attributes of a View Object

<strong>1 Row Updated by Me in the method applyVCforGlobalSearchEmployees</strong>

partial trigger has been added to element what should be updated by clicking on button.

    partialTriggers="::b1"

<br/>

### ADF 11g : Implementing Search for Multiple Attributes of a View Object

Implementing Search for Multiple Attributes of a View Object

Original:  
https://technology.amis.nl/2011/04/05/adf-11g-implementing-search-for-multiple-attributes-of-a-view-object/

Today I was asked to build a search component that searches a view object for all occurrences of a search string entered by the user. It should look like the ADF Quick Query Component, however, without choice for the attributes to search on. All attributes are search-able. I choose to combine ViewCriteria and custom method on the Application Module. Here is how I did it.

I started with the creation of Default Business Components for the Employees table. Next I created a Bindvariable to represent the value entered by the user.

<div align="center">
    <img src="/img/Implementing-Search-for-Multiple-Attributes-of-a-View-Object/pic1.jpg" alt="Implementing Search for Multiple Attributes of a View Object">
</div>

After that I created view Criteria containing ‘OR’ conjunctions and ‘CONTAINS’ clauses for all attributes that I need to search on. The image below depicts what the view criteria look like.

<div align="center">
    <img src="/img/Implementing-Search-for-Multiple-Attributes-of-a-View-Object/pic2.jpg" alt="Implementing Search for Multiple Attributes of a View Object">
</div>

Final part in the ADF-BC project is to create a custom method on the Application Module to apply view criteria to the view object and to apply the user entered value to the bind variable.

    public void applyVCforGlobalSearchEmployees(String theString){
       ViewObjectImpl vo = getEmployeesView1();
       ViewCriteria vc = vo.getViewCriteria("EmployeesViewCriteria");
       vc.resetCriteria();
       VariableValueManager vvm = vc.ensureVariableManager();
       vvm.setVariableValue("globalSearchString", theString);
       vo.applyViewCriteria(vc,true);
       vo.executeQuery();
    }

Publish this method to the client so we can use it to search.

<div align="center">
    <img src="/img/Implementing-Search-for-Multiple-Attributes-of-a-View-Object/pic3.jpg" alt="Implementing Search for Multiple Attributes of a View Object">
</div>

The published method is now ready to be used on the page (assuming you created one). If not, just create a simple page containing a table based on the employees collection. Drop the method as an ADF Parameter form.

<div align="center">
    <img src="/img/Implementing-Search-for-Multiple-Attributes-of-a-View-Object/pic4.jpg" alt="Implementing Search for Multiple Attributes of a View Object">
</div>

If all is ok, JDeveloper will take care of the partial triggering to refresh the table after the button has been pressed. For the looks, I added a custom icon to the button,adjusted the labels and prompts and moved to component to a toolbar facet.

<div align="center">
    <img src="/img/Implementing-Search-for-Multiple-Attributes-of-a-View-Object/pic5.jpg" alt="Implementing Search for Multiple Attributes of a View Object">
</div>
