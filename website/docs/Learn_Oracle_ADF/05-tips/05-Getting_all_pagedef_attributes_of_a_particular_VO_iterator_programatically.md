---
layout: page
title: ADF UI - Getting all pagedef attributes of a particular VO iterator programatically
permalink: /learn-oracle-adf/tips/Getting-all-pagedef-attributes-of-a-particular-VO-iterator-programatically/
---


# ADF UI - Getting all pagedef attributes of a particular VO iterator programatically



This might be rare requirement to get handle on pagedef attributes of a particular VO iterator. But, it would be quite useful for some scenarios like when you want to get the values of attributes of a particular iterator present in the current jsff page (not each and every VO attribute) and compare them with the original values to know if the user has changed the attribute through the UI. It would be waste of effort to check all VO attributes if they're changed as the user can only change the attributes present in the UI jsff pages.


But, it's not as easy as getting a reference to bindings (DCBindingContainer) and call the method getAttributeBindngs() on it. Even though it returns all the pagedef attribute bindings, they'll include attributes of all VO iterators in the pagedef. The tricky part is to get attributes of only a particular VO iterator in the pagedef. Here, in this post, I'll explain how to do that.



I did some research and wrote the a method to retrieve all the pagedef attributes of the VO iterator specified by 'VOName' as a parameter. This method also has the logic to find out the transient attributes present the VO. This would be quite useful when you want to find out the transient attributes of a VOIterator in the pagedef and to do logic on them (like skipping them for comparision, etc.,).



    public void storePageDefAttrs(String VOName) {
        DCBindingContainer binding =
            (DCBindingContainer)ADFUtil.evaluateEL("#{bindings}");
        //getting the attribute bindings in the pagedef, it'll contain all pagedef attribute bindings of all VO iterators in pagedef
        List attrBindings = binding.getAttributeBindings();
        //getting iterator for attribute bindings
        Iterator itr = attrBindings.iterator();
        //getting the current row for the VOName
        ViewRowImpl row =
            (ViewRowImpl)ADFUtil.evaluateEL("#{bindings." + VOName +
                                            "Iterator.currentRow}");
        //pageDefAttrs will store pageDef attribute names for the given VO specified by 'VOName'
        List pageDefAttrs = new ArrayList();
        //transientAttrs will store trasient attribute names added in the pagedef for the given VO specified by 'VOName'
        List transientAttrs = new ArrayList();
        //iterating through all attribute bindings
        while (itr.hasNext()) {
            AttributeBinding attrBinding = (AttributeBinding)itr.next();
            //getting the attribute name
            String attrName = attrBinding.getName();
            //getting the iterator binding for the attribute
            DCIteratorBinding attrIterBinding =
                (DCIteratorBinding)ADFUtil.evaluateEL("#{bindings." +
                                                      attrName +
                                                      ".iteratorBinding}");
            //getting the iterator  name for the iterator binding
            String attrIterBidningName = attrIterBinding.getVOName();
            //checking if this attribute's iterator name is equal to the passed VOName
            if (VOName.equals(attrIterBidningName)) {
                //if the attributes' iterator  name is equal to passed VOName, this attributes belongs to the VO specified by 'VOName'. So adding it to pageDefAttrs.
                pageDefAttrs.add(attrName);
                //getting the attribute index
                int attrIndex = row.getAttributeIndexOf(attrName);
                //getting the view attribute definition
                ViewAttributeDefImpl vDef =
                    (ViewAttributeDefImpl)row.getViewDef().getAttributeDef(attrIndex);
                //checking if it's a transient attribute.
                boolean transientPageDefAttr =
                    (vDef.getEntityAttributeDef() == null) ||
                    (vDef.getEntityAttributeDef().getColumnName() == null);
                //if it's transient, adding it to transientAttrs list
                if (transientPageDefAttr) {
                    transientAttrs.add(attrName);
                }
            }
        }
        //stroing the pagedef attribute names and transient attribute names for the given VOName in the pageFlowScope variables.
        ADFUtil.setEL("#{pageFlowScope.pageDefAttrs}", pageDefAttrs);
        ADFUtil.setEL("#{pageFlowScope.trasientPageDefAttrs}", transientAttrs);
        this.setPageDeAttrs(pageDefAttrs);
    }




<br/><br/><br/><br/>
http://www.adftips.com/2010/11/adf-ui-getting-pagedef-attributes.html
