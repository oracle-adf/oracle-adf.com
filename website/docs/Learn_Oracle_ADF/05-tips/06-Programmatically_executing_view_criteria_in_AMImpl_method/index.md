---
layout: page
title: ADF Model Programmatically executing view criteria in AMImpl method
permalink: /learn-oracle-adf/tips/Programmatically-executing-view-criteria-in-AMImpl-method/
---


<h3>ADF Model: Programmatically executing view criteria in AMImpl method
</h3>


    public void getEmployeesInDept(Integer deptId) {
        if (deptId == null || deptId == 0) {
            ViewObjectImpl empDeptVO = this.getEmpDeptVO();
            //View Criteria without bind variable
            ViewCriteria vc = empDeptVO.getViewCriteria("noRowsVC");
            empDeptVO.applyViewCriteria(vc);
            empDeptVO.executeQuery();
        } else {
            ViewObjectImpl empDeptVO = this.getEmpDeptVO();
            //View Criteria with bind variable 'Bind_deptId'
            empDeptVO.setApplyViewCriteriaName("findByDeptId");
            empDeptVO.setNamedWhereClauseParam("Bind_deptId", deptId);
            empDeptVO.executeQuery();
        }
    }



The above method shows how to execute view criterias "noRowsVC"(with no bind variables) and "findByDeptId"(which has a bind variable "Bind_deptId") in EmpDeptVO. The below screenshots show the view criterias in EmpDeptVO.


That's it. The method executes the VC and gets the query results. You can expose this method in client's interface and add this method in jsff pagedef file as a methodAction binding. The later post shows how to pass parameters and call/execute this AMImpl method ( i.e., getEmployeesInDept) in UI project's java bean.


<img src="/website/docs/Learn_Oracle_ADF/05-tips/06-Programmatically_executing_view_criteria_in_AMImpl_method/Programmatically_executing_view_criteria_in_AMImpl_method_1.jpg" border="0" alt="Programmatically executing view criteria in AMImpl method">

<br/><br/>

<img src="/website/docs/Learn_Oracle_ADF/05-tips/06-Programmatically_executing_view_criteria_in_AMImpl_method/Programmatically_executing_view_criteria_in_AMImpl_method_2.jpg" border="0" alt="Programmatically executing view criteria in AMImpl method">



<br/><br/><br/><br/>
http://www.adftips.com/2010/10/adf-model-programmatically-executing.html
