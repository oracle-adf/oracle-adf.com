---
layout: page
title: How to execute a method before the jsf page gets loaded
permalink: /learn-oracle-adf/tips/how-to-execute-a-method-before-the-jsf-page-gets-loaded/
---



# How to execute a method before the jsf page gets loaded


If you want to execute a method just before entering a page(jsff or jspx) in a task flow, just follow the below steps.


1. Go to page definition of the jsf page. In the bindings section, add the methodAction to the method which you want to execute.


2. In the executables section, add 'invokeAction' and then, in the dialog specify some id and select the method action which you added in bindings for the 'Binds' property.


3. Now, select the added action and click 'Edit' icon and specify the 'Refresh' property as 'ifNeeded' ( By default the value for 'Refresh' will be 'deferred').


Thats it. When you run the page, the method will be executed just before the actual page loads. So, if you want to execute some method(s) before loading a page, you can follow these steps.


<br/><br/><br/><br/>
http://www.adftips.com/search?updated-min=2009-01-01T00:00:00-08:00&updated-max=2010-01-01T00:00:00-08:00&max-results=3
