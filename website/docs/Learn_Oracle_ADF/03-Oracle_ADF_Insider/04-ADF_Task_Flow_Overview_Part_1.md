---
layout: page
title: ADF Task Flow - Overview Part 1
permalink: /learn-oracle-adf/oracle-adf-insider/adf_task_flow_overview_part_1/
---

# ADF Task Flow - Overview Part 1


<br/>
<br/>

<div align="center">

<iframe width="640" height="360" src="http://www.youtube.com/embed/A3CmDhWHaG0" frameborder="0" allowfullscreen></iframe>

</div>

<br/>
<br/>

<div align="center">

<br/>
<br/>
<br/>
<br/>

<img src="/website/docs/Learn_Oracle_ADF/03-Oracle_ADF_Insider/ADF_Task_Flow_Overview_Part_1/ADF_Task_Flow_Overview_Part_1_01.png" border="0" alt="Oracle ADF Task Flow"><br/><br/><br/><br/>
<img src="/website/docs/Learn_Oracle_ADF/03-Oracle_ADF_Insider/ADF_Task_Flow_Overview_Part_1/ADF_Task_Flow_Overview_Part_1_02.png" border="0" alt="Oracle ADF Task Flow"><br/><br/><br/><br/>
<img src="/website/docs/Learn_Oracle_ADF/03-Oracle_ADF_Insider/ADF_Task_Flow_Overview_Part_1/ADF_Task_Flow_Overview_Part_1_03.png" border="0" alt="Oracle ADF Task Flow"><br/><br/><br/><br/>
<img src="/website/docs/Learn_Oracle_ADF/03-Oracle_ADF_Insider/ADF_Task_Flow_Overview_Part_1/ADF_Task_Flow_Overview_Part_1_04.png" border="0" alt="Oracle ADF Task Flow"><br/><br/><br/><br/>
<img src="/website/docs/Learn_Oracle_ADF/03-Oracle_ADF_Insider/ADF_Task_Flow_Overview_Part_1/ADF_Task_Flow_Overview_Part_1_05.png" border="0" alt="Oracle ADF Task Flow"><br/><br/><br/><br/>
<img src="/website/docs/Learn_Oracle_ADF/03-Oracle_ADF_Insider/ADF_Task_Flow_Overview_Part_1/ADF_Task_Flow_Overview_Part_1_06.png" border="0" alt="Oracle ADF Task Flow"><br/><br/><br/><br/>
<img src="/website/docs/Learn_Oracle_ADF/03-Oracle_ADF_Insider/ADF_Task_Flow_Overview_Part_1/ADF_Task_Flow_Overview_Part_1_07.png" border="0" alt="Oracle ADF Task Flow"><br/><br/><br/><br/>
<img src="/website/docs/Learn_Oracle_ADF/03-Oracle_ADF_Insider/ADF_Task_Flow_Overview_Part_1/ADF_Task_Flow_Overview_Part_1_08.png" border="0" alt="Oracle ADF Task Flow"><br/><br/><br/><br/>
<img src="/website/docs/Learn_Oracle_ADF/03-Oracle_ADF_Insider/ADF_Task_Flow_Overview_Part_1/ADF_Task_Flow_Overview_Part_1_09.png" border="0" alt="Oracle ADF Task Flow"><br/><br/><br/><br/>


<img src="/website/docs/Learn_Oracle_ADF/03-Oracle_ADF_Insider/ADF_Task_Flow_Overview_Part_1/ADF_Task_Flow_Overview_Part_1_10.png" border="0" alt="Oracle ADF Task Flow"><br/><br/><br/><br/>
<img src="/website/docs/Learn_Oracle_ADF/03-Oracle_ADF_Insider/ADF_Task_Flow_Overview_Part_1/ADF_Task_Flow_Overview_Part_1_11.png" border="0" alt="Oracle ADF Task Flow"><br/><br/><br/><br/>
<img src="/website/docs/Learn_Oracle_ADF/03-Oracle_ADF_Insider/ADF_Task_Flow_Overview_Part_1/ADF_Task_Flow_Overview_Part_1_12.png" border="0" alt="Oracle ADF Task Flow"><br/><br/><br/><br/>
<img src="/website/docs/Learn_Oracle_ADF/03-Oracle_ADF_Insider/ADF_Task_Flow_Overview_Part_1/ADF_Task_Flow_Overview_Part_1_13.png" border="0" alt="Oracle ADF Task Flow"><br/><br/><br/><br/>
<img src="/website/docs/Learn_Oracle_ADF/03-Oracle_ADF_Insider/ADF_Task_Flow_Overview_Part_1/ADF_Task_Flow_Overview_Part_1_14.png" border="0" alt="Oracle ADF Task Flow"><br/><br/><br/><br/>
<img src="/website/docs/Learn_Oracle_ADF/03-Oracle_ADF_Insider/ADF_Task_Flow_Overview_Part_1/ADF_Task_Flow_Overview_Part_1_15.png" border="0" alt="Oracle ADF Task Flow"><br/><br/><br/><br/>
<img src="/website/docs/Learn_Oracle_ADF/03-Oracle_ADF_Insider/ADF_Task_Flow_Overview_Part_1/ADF_Task_Flow_Overview_Part_1_16.png" border="0" alt="Oracle ADF Task Flow"><br/><br/><br/><br/>
<img src="/website/docs/Learn_Oracle_ADF/03-Oracle_ADF_Insider/ADF_Task_Flow_Overview_Part_1/ADF_Task_Flow_Overview_Part_1_17.png" border="0" alt="Oracle ADF Task Flow"><br/><br/><br/><br/>
<img src="/website/docs/Learn_Oracle_ADF/03-Oracle_ADF_Insider/ADF_Task_Flow_Overview_Part_1/ADF_Task_Flow_Overview_Part_1_18.png" border="0" alt="Oracle ADF Task Flow"><br/><br/><br/><br/>
<img src="/website/docs/Learn_Oracle_ADF/03-Oracle_ADF_Insider/ADF_Task_Flow_Overview_Part_1/ADF_Task_Flow_Overview_Part_1_19.png" border="0" alt="Oracle ADF Task Flow"><br/><br/><br/><br/>


<img src="/website/docs/Learn_Oracle_ADF/03-Oracle_ADF_Insider/ADF_Task_Flow_Overview_Part_1/ADF_Task_Flow_Overview_Part_1_20.png" border="0" alt="Oracle ADF Task Flow"><br/><br/><br/><br/>
<img src="/website/docs/Learn_Oracle_ADF/03-Oracle_ADF_Insider/ADF_Task_Flow_Overview_Part_1/ADF_Task_Flow_Overview_Part_1_21.png" border="0" alt="Oracle ADF Task Flow"><br/><br/><br/><br/>
<img src="/website/docs/Learn_Oracle_ADF/03-Oracle_ADF_Insider/ADF_Task_Flow_Overview_Part_1/ADF_Task_Flow_Overview_Part_1_22.png" border="0" alt="Oracle ADF Task Flow"><br/><br/><br/><br/>
<img src="/website/docs/Learn_Oracle_ADF/03-Oracle_ADF_Insider/ADF_Task_Flow_Overview_Part_1/ADF_Task_Flow_Overview_Part_1_23.png" border="0" alt="Oracle ADF Task Flow"><br/><br/><br/><br/>
<img src="/website/docs/Learn_Oracle_ADF/03-Oracle_ADF_Insider/ADF_Task_Flow_Overview_Part_1/ADF_Task_Flow_Overview_Part_1_24.png" border="0" alt="Oracle ADF Task Flow"><br/><br/><br/><br/>
<img src="/website/docs/Learn_Oracle_ADF/03-Oracle_ADF_Insider/ADF_Task_Flow_Overview_Part_1/ADF_Task_Flow_Overview_Part_1_25.png" border="0" alt="Oracle ADF Task Flow"><br/><br/><br/><br/>
<img src="/website/docs/Learn_Oracle_ADF/03-Oracle_ADF_Insider/ADF_Task_Flow_Overview_Part_1/ADF_Task_Flow_Overview_Part_1_26.png" border="0" alt="Oracle ADF Task Flow"><br/><br/><br/><br/>
<img src="/website/docs/Learn_Oracle_ADF/03-Oracle_ADF_Insider/ADF_Task_Flow_Overview_Part_1/ADF_Task_Flow_Overview_Part_1_27.png" border="0" alt="Oracle ADF Task Flow"><br/><br/><br/><br/>
<img src="/website/docs/Learn_Oracle_ADF/03-Oracle_ADF_Insider/ADF_Task_Flow_Overview_Part_1/ADF_Task_Flow_Overview_Part_1_28.png" border="0" alt="Oracle ADF Task Flow"><br/><br/><br/><br/>
<img src="/website/docs/Learn_Oracle_ADF/03-Oracle_ADF_Insider/ADF_Task_Flow_Overview_Part_1/ADF_Task_Flow_Overview_Part_1_29.png" border="0" alt="Oracle ADF Task Flow"><br/><br/><br/><br/>



<img src="/website/docs/Learn_Oracle_ADF/03-Oracle_ADF_Insider/ADF_Task_Flow_Overview_Part_1/ADF_Task_Flow_Overview_Part_1_30.png" border="0" alt="Oracle ADF Task Flow"><br/><br/><br/><br/>
<img src="/website/docs/Learn_Oracle_ADF/03-Oracle_ADF_Insider/ADF_Task_Flow_Overview_Part_1/ADF_Task_Flow_Overview_Part_1_31.png" border="0" alt="Oracle ADF Task Flow"><br/><br/><br/><br/>
<img src="/website/docs/Learn_Oracle_ADF/03-Oracle_ADF_Insider/ADF_Task_Flow_Overview_Part_1/ADF_Task_Flow_Overview_Part_1_32.png" border="0" alt="Oracle ADF Task Flow"><br/><br/><br/><br/>
<img src="/website/docs/Learn_Oracle_ADF/03-Oracle_ADF_Insider/ADF_Task_Flow_Overview_Part_1/ADF_Task_Flow_Overview_Part_1_33.png" border="0" alt="Oracle ADF Task Flow"><br/><br/><br/><br/>
<img src="/website/docs/Learn_Oracle_ADF/03-Oracle_ADF_Insider/ADF_Task_Flow_Overview_Part_1/ADF_Task_Flow_Overview_Part_1_34.png" border="0" alt="Oracle ADF Task Flow"><br/><br/><br/><br/>
<img src="/website/docs/Learn_Oracle_ADF/03-Oracle_ADF_Insider/ADF_Task_Flow_Overview_Part_1/ADF_Task_Flow_Overview_Part_1_35.png" border="0" alt="Oracle ADF Task Flow"><br/><br/><br/><br/>
<img src="/website/docs/Learn_Oracle_ADF/03-Oracle_ADF_Insider/ADF_Task_Flow_Overview_Part_1/ADF_Task_Flow_Overview_Part_1_36.png" border="0" alt="Oracle ADF Task Flow"><br/><br/><br/><br/>
<img src="/website/docs/Learn_Oracle_ADF/03-Oracle_ADF_Insider/ADF_Task_Flow_Overview_Part_1/ADF_Task_Flow_Overview_Part_1_37.png" border="0" alt="Oracle ADF Task Flow"><br/><br/><br/><br/>
<img src="/website/docs/Learn_Oracle_ADF/03-Oracle_ADF_Insider/ADF_Task_Flow_Overview_Part_1/ADF_Task_Flow_Overview_Part_1_38.png" border="0" alt="Oracle ADF Task Flow"><br/><br/><br/><br/>
<img src="/website/docs/Learn_Oracle_ADF/03-Oracle_ADF_Insider/ADF_Task_Flow_Overview_Part_1/ADF_Task_Flow_Overview_Part_1_39.png" border="0" alt="Oracle ADF Task Flow"><br/><br/><br/><br/>



<img src="/website/docs/Learn_Oracle_ADF/03-Oracle_ADF_Insider/ADF_Task_Flow_Overview_Part_1/ADF_Task_Flow_Overview_Part_1_40.png" border="0" alt="Oracle ADF Task Flow"><br/><br/><br/><br/>
<img src="/website/docs/Learn_Oracle_ADF/03-Oracle_ADF_Insider/ADF_Task_Flow_Overview_Part_1/ADF_Task_Flow_Overview_Part_1_41.png" border="0" alt="Oracle ADF Task Flow"><br/><br/><br/><br/>
<img src="/website/docs/Learn_Oracle_ADF/03-Oracle_ADF_Insider/ADF_Task_Flow_Overview_Part_1/ADF_Task_Flow_Overview_Part_1_42.png" border="0" alt="Oracle ADF Task Flow"><br/><br/><br/><br/>
<img src="/website/docs/Learn_Oracle_ADF/03-Oracle_ADF_Insider/ADF_Task_Flow_Overview_Part_1/ADF_Task_Flow_Overview_Part_1_43.png" border="0" alt="Oracle ADF Task Flow"><br/><br/><br/><br/>
<img src="/website/docs/Learn_Oracle_ADF/03-Oracle_ADF_Insider/ADF_Task_Flow_Overview_Part_1/ADF_Task_Flow_Overview_Part_1_44.png" border="0" alt="Oracle ADF Task Flow"><br/><br/><br/><br/>
<img src="/website/docs/Learn_Oracle_ADF/03-Oracle_ADF_Insider/ADF_Task_Flow_Overview_Part_1/ADF_Task_Flow_Overview_Part_1_45.png" border="0" alt="Oracle ADF Task Flow"><br/><br/><br/><br/>
<img src="/website/docs/Learn_Oracle_ADF/03-Oracle_ADF_Insider/ADF_Task_Flow_Overview_Part_1/ADF_Task_Flow_Overview_Part_1_46.png" border="0" alt="Oracle ADF Task Flow"><br/><br/><br/><br/>
<img src="/website/docs/Learn_Oracle_ADF/03-Oracle_ADF_Insider/ADF_Task_Flow_Overview_Part_1/ADF_Task_Flow_Overview_Part_1_47.png" border="0" alt="Oracle ADF Task Flow"><br/><br/><br/><br/>
<img src="/website/docs/Learn_Oracle_ADF/03-Oracle_ADF_Insider/ADF_Task_Flow_Overview_Part_1/ADF_Task_Flow_Overview_Part_1_48.png" border="0" alt="Oracle ADF Task Flow"><br/><br/><br/><br/>
<img src="/website/docs/Learn_Oracle_ADF/03-Oracle_ADF_Insider/ADF_Task_Flow_Overview_Part_1/ADF_Task_Flow_Overview_Part_1_49.png" border="0" alt="Oracle ADF Task Flow"><br/><br/><br/><br/>


<img src="/website/docs/Learn_Oracle_ADF/03-Oracle_ADF_Insider/ADF_Task_Flow_Overview_Part_1/ADF_Task_Flow_Overview_Part_1_50.png" border="0" alt="Oracle ADF Task Flow"><br/><br/><br/><br/>
<img src="/website/docs/Learn_Oracle_ADF/03-Oracle_ADF_Insider/ADF_Task_Flow_Overview_Part_1/ADF_Task_Flow_Overview_Part_1_51.png" border="0" alt="Oracle ADF Task Flow"><br/><br/><br/><br/>
<img src="/website/docs/Learn_Oracle_ADF/03-Oracle_ADF_Insider/ADF_Task_Flow_Overview_Part_1/ADF_Task_Flow_Overview_Part_1_52.png" border="0" alt="Oracle ADF Task Flow"><br/><br/><br/><br/>
