---
layout: page
title: Oracle ADF Insider - Learn Oracle ADF
permalink: /learn-oracle-adf/oracle-adf-insider/
---

# Oracle ADF Insider - Learn Oracle ADF

<br/>
<strong>Oracle ADF Insider  - Learn Oracle ADF Online</strong><br/>
http://www.oracle.com/technetwork/developer-tools/adf/learnmore/adfinsider-093342.html


<strong>Advanced ADF Series Part 1</strong><br/>
https://apex.oracle.com/pls/apex/f?p=44785:24:0::NO::P24_CONTENT_ID,P24_PREV_PAGE:6022,1


<strong>Advanced ADF Series Part 2</strong><br/>
https://apex.oracle.com/pls/apex/f?p=44785:24:34382417150535:::24:P24_CONTENT_ID,P24_PREV_PAGE:6326,24



<br/>
<h3>Oracle ADF Insider Basics</h3>


<a href="/learn-oracle-adf/oracle-adf-insider/introduction-t0-oracle-adf/">Introduction to Oracle ADF</a><br/>
Learn what Oracle ADF is all about, understand how ADF fits into the Oracle Fusion architecture, what solutions does it offer, and how it simplifies application development.


**Introduction to Oracle ADF Business Components**

Oracle ADF Business Components offer a simple way to create reusable business services that interact with your relational database. Learn about the architecture and features that are provided by Oracle ADF Business Components.




**Introduction to Oracle ADF Faces Components**

Oracle ADF Faces offers a set of rich Ajax enabled components that simplify the creation of Web user interfaces. Get an overview of the various components and the built-in capabilities of the Oracle ADF Faces framework.


**Guide to the JDeveloper IDE**

A quick Don't PANIC guide to the basic features of the JDeveloper IDE including window manipulation and the 6 core windows that developers will work with.


**Guide to the JDeveloper IDE Editor**

A fighter pilot's guide to the rich features in the JDeveloper IDE Editor, discussing features that assist programmers to locate and edit their code faster, productivity boosters to save typing and fixing typos, features to keep code neat and tidy so the whole team can read the code, and navigation functions to move around a large code base without getting lost.


<br/>
<h3>Oracle ADF Insider Advanced</h3>


<a href="/learn-oracle-adf/oracle-adf-insider/adf-bindings-internals-part-1/">ADF Binding Internals - Part 1</a><br/>
Looking at understanding how the ADF Model layer works?  What are data controls?  What are bindings?  In part 1 of this ADF Insider you will learn about data controls, the binding layer and the various files (e.g. DataBindings.cpx, PageDef.xml) that are used to bind business services to UI controls.


<a href="/learn-oracle-adf/oracle-adf-insider/adf-bindings-internals-part-2/">ADF Binding Internals - Part 2</a><br/>
In the second part of this ADF Insider presentation you will learn about how you access ADF Model bindings at runtime.  Understand what is a BindingContext, a BindingContainer and the common methods you will use. Find out how you can use EL and Java to access an AttributeBinding, an OperationBinding and a JUCtrlListBinding.


<a href="/learn-oracle-adf/oracle-adf-insider/adf_task_flow_overview_part_1/">ADF Task Flow - Overview (Part 1)</a><br/>

This 90 minute recording introduces the concept of ADF unbounded and bounded task flows, as well as other ADF Controller features. The session starts with an overview of unbounded task flows, bounded task flows and the different activities that exist for developers to build complex application flows. Exception handling and the Train navigation model is also covered in this first part of a two part series. By example of developing a sample application, the recording guides viewers through building unbounded and bounded task flows. This session is continued in a second part.



<a href="/learn-oracle-adf/oracle-adf-insider/adf_task_flow_overview_part_2/">ADF Task Flow - Overview (Part 2)</a><br/>

This 75 minute session continues where part 1 ended and completes the sample application that guides viewers through different aspects of unbounded and bounded task flow development. In this recording, memory scopes, save for later, task flow opening in dialogs and remote task flow calls are explained and demonstrated. If you are new to ADF Task Flow, then it is recommended to first watch part 1 of this series to be able to follow the explanation guided by the sample application.




ADF Application Security<br/>
ADF Security is an authentication and authorization framework in Oracle ADF. This session explains ADF Security from ground up. It starts with container managed security and JAAS to how they to fit into ADF Security. It then explains the ADF Security architecture with Oracle Platform Security Services (OPSS) and the WebLogic security platform for authentication. Topics discussed include authorization, page protection, task flow protection as well as security expressions ADF provides for EL.




Deploying ADF Security-Enabled Applications to a Standalone WebLogic Domain - Part 1<br/>
In Part 1 of this series you'll learn how to package your application in an ear file, start Oracle WebLogic Server Domain as well as Oracle Internet Directory (OID), setup OID authenticator, setup a jdbc datasource and deploy your ADF application using Oracle FMW Control.



Deploying ADF Security-Enabled Applications to a Standalone WebLogic Domain - Part 2<br/>
This session, Part 2, picks up where Part 1 of this series left off by showing you how to map application roles to the OID groups and test the application, update role permissions at runtime and redeploy the application with different security-related settings.



Enabling SSO for the ADF application using Oracle Access Manager 11g<br/>
In this session you'll learn how to enable SSO in an ADF application through the use of Oracle Access Manager 11g. Specifically you'll walk away with an understanding of the installation process for Oracle HTTP Server 11g (OHS) as a reverse proxy for Oracle WebLogic Server, the installation process of Oracle Access Manager 11g (OAM) Web Gate within OHS and then enable SSO through OAM.


Using Groovy in ADF Application<br/>
Groovy is a Java-like scripting language that gives you huge amounts of flexibility when building ADF applications.  In this ADF Insider you will see how Groovy can be used in ADF Business Components to define default values, validation and dynamic error messages.


ADF Faces Skinning<br/>
Skinning in ADF Faces enables application developers to change  the color scheme of an application without changing the application itself. To build a skin, developers use Cascading Style Sheets (CSS) referenced from the trinidad-skins.xml file and configured in the trinidad-config.xml file. The session explains how skinning works, starting from skin selectors and their usage.



<a href="/learn-oracle-adf/oracle-adf-insider/using-javascript-in-oracle-adf-faces/">Using JavaScript in Oracle ADF Faces</a><br/>

ADF Faces provides developers with a component oriented client side JavaScript architecture that provides a consistent development environment across browsers. This session explains the server and client architecture of ADF Faces and how developers can use JavaScript in their ADF Faces applications. It explains client listeners, server listeners (Ajax calls to the server) and calls from Java to the client.

ADF Page Templates and Declarative Components<br/>
Learn how you can build a consistent look and feel to your ADF application pages using page templates and declarative components.  In this session you will see how you can use template facets to define content areas in a template, and how attributes can be used to control features of the template such as initial layout.  You will also see how you can build reusable composite components.

A closer look at integrating SOA and ADF<br/>
Oracle ADF has built in integration with the Oracle SOA Suite. In this session we'll show you how to expose your business services to SOA consumers, and how to integrate SOA processes into your ADF application.

<a href="/learn-oracle-adf/oracle-adf-insider/adf-region-interaction-an-overview/">ADF Region Interaction - An Overview</a><br/>
This session covers most of the options that exist for communicating between regions. It briefly discusses what it takes to build regions from bounded task flows before going into details using slides and samples. The following interaction is explained: contextual events, queue action in region, input parameters and PPR, drag and drop, shared Data Controls, parent action and region navigation listener.

ADF Region Interaction - Contextual Events<br/>
Contextual events is a power solution for inter region communication and can be used as a communication channel between a parent view and its contained regions, as well as between regions. This session explains how to set up contextual events, how to define producers and event listeners and how to define the payload message. In a live example the session shows developers the building process too.

Classic Mistakes with Oracle ADF Internal APIs - Part 1<br/>
This session focuses on classical mistakes developers make when working with the Oracle ADF binding layer and the ADF Controller (Task Flows). It shows examples of how not to do it and the correct way of using ADF in Oracle JDeveloper 11g.

Classic Mistakes with Oracle ADF Internal APIs - Part 2<br/>
This second part of classic API mistakes focuses on Expression Language problems, the use of ADF Faces and JavaScript. Developers learn best practices and pitfalls to avoid.

Project Considerations for Team Development<br/>
This session covers many aspects of team development including how to set up your team's resources for developing projects and how to use source control repositories in a team environment. Additionally, an overview of the features of Team Productivity Center is provided. Finally, several considerations for reusing code are covered, including best practices for setting up business components and using ADF libraries to distribute shared code.

Redeveloping an Oracle Forms Application with Oracle JDeveloper and Oracle ADF<br/>
Many customers developing ADF applications may be looking to re-implement applications originally built using Oracle Forms. In this presentation you will hear about the options for redevelopment, and see demonstrations of how you can re-implement common Forms features in ADF based on a redevelopment case study.


Building Visually Appealing Web 2.0 Data Dashboards<br/>
This session will show you how to turn data into information by presenting it visually to your users. You'll learn how you can turn simple queries into powerful graphs, maps, pivot tables, and other dynamic graphical representations.

Oracle ADF provides a mechanism for implementing and viewing logging information as an aid to debugging<br/>
This presentation shows how you can use ADFLogger and the dashboard to view logging information.


Building User Interfaces<br/>
This session explains the process of developing layouts with ADF Faces. Prototyping strategies are discussed, including ADF's own Placeholder Data Control. The remainder of the session covers the ADF Faces layout components in detail, highlighting their uses and providing examples of how the components might work in various scenarios. Finally, a demonstration shows how to work with layout components to define the way nested ADF Faces content appears.

In a Nutshell: The ADF UI Shell<br/>
Beyond the basic training on Oracle's JDeveloper 11g ADF, developers are left with an overwhelming list of options for presenting a rich application to users. Buttons, sliders, graphs, and more. Yet individually, these don't make an application, these don't make a user experience; how will your application pull this into a consistent look and feel, how will the application flow, how will they navigate in your application? This session looks at Oracle's ADF UI Shell (a.k.a. Dynamic Tabs Shell), a working combination of page templates, navigation controls, and application structure bringing the many parts of 11g ADF applications together into a living application experience.

Using Apache JMeter to load test ADF applications<br/>
Apache JMeter can be used to run performance, load and stress tests on ADF applications. However, JMeter has no intelligence on understanding ADF applications and requires careful configuration to process the ADF cookies and HTTP state variables. This presentation shows how to configure JMeter for ADF testing, demonstrates a live stress test, and highlights some gotchas that can break the tests you create.


One-Size-Doesn't-Fit-All: Oracle ADF Architectural Fundamentals<br/>
Oracle Application Development Framework (Oracle ADF) practitioners will quickly realize that there’s no one-size-fits-all approach to creating an Oracle ADF application architecture. The unique challenges and requirements of your system will have a strong influence on the solution you finally build. Taking a step back from the nitty-gritty details of Oracle ADF, this presentation looks at the early decisions you need to make and what architectural choices you have in meeting your requirements.

Integrating SOAP and REST Services in Oracle ADF<br/>
Oracle ADF Integrates with SOAP and REST services as a service client and service provider. This ADF Insider session shows the options you have for accessing remote SOAP and REST services from ADF and the best practices involved with this. In its second part, this session shows the available options to make ADF Business Components models accessible to SOAP and REST clients. Exposing ADF BC as a service provider allows you to support multi-channel access, e.g. from Web, SOA and ADF mobile client, to your business logic and data.


<br/>
<h3>Oracle ADF Insider Essentials</h3>

<br/>
<strong>ADF Business Components</strong>

Programmatically iterating through rows of a view object<br/>
A common use case is to be able to programmatic access the rows in a view object. This ADF Insider Essentials shows you how.


Building a dependent list of values<br/>
Find out how you can use the declarative features of ADF Business Components to build a dependent list of values that automatically limit its choices based on another attribute value.

Implementing Sequences in an ADF Applications<br/>
Most, if not all, ADF applications based on database tables will have to manage assigning sequence numbers to unique and primary key attribute values. Find out how you can do this in ADF Business Components.


Extending ADF Framework Security with Resource Permissions<br/>
This sample shows how to use the OPSS Resource Permission in ADF Security to protect an entity attribute from update when an entity row is newly created.

Programmatical access a view criteria<br/>
View criteria allow you to define parameterized filters on a view object. This ADF Insider Essentials explains how you can apply, remove, and append view criteria from code.

Setting a row as non editable based on a value<br/>
In this ADF Insider Essentials you will see how you can set a row of data to be non editable depending on the value in another field or attribute.


ADF BC Model Driven LOV Switcher<br/>
In this ADF Insider Essentials you will learn a feature of model driven list-of-values where the LOV definition can be switched at runtime. This is usually triggered by data in a related view object attribute.


Referencing a default value from an attribute in a different EO<br/>
In this ADF Insider Essentials you will learn how you can default an attribute's value with a value populated from a different entity object. For example, an order item price is defaulted to the suggested wholesale price for the product.


Implementing a total<br/>
Often in an application you will want to display a sum or total of a particular attribute, for example the total salaries in a department. In this ADF Insider Essential you will see how you can implement using a Groovy expression.


Building a test client for testing ADF Business Components<br/>
ADF Business Components can be tested in the ADF Model Tester or in simple Test Client classes. These Test Client classes can work with generic view objects or set and use bind variables and view criteria. This ADF Insider shows you how to write the class using a template and set all the appropriate component values.


Basing ADF Business Component View Objects on more than one Entity Object<br/>
Tables are littered with foreign keys that have little meaning to users. Often we want to show fields from other tables with more meaningful values to the user, and, if the user changes the foreign key then the lookup values are instantly changed. This presentation will show you how to create ADF Business Component View Objects where the View Object is based on more than one Entity Object: An updatable  Entity Object and a reference lookup Entity Object.

Retrieving the previous value of an ADF BC attribute<br/>
Sometimes we will have a requirement to show the previous value of an ADF BC attribute, or even create a business rule that needs to compare an old and new value for an attribute. You will learn how easy it is to do this in this ADF Insider Essential.


<br/>
<br/>
<strong>ADF Model</strong>
<br/>

Accessing ADF Binding Layer from Java<br/>
When writing code in a managed bean you might want to access attribute values or execute method calls. This ADF Insider Essentials shows you how you can access ADF bindings from a managed bean.

Synchronizing a Tree Control with Detail Data<br/>
This ADF Insider Essentials shows you how selecting a node in a UI tree component can automatically synchronize that data with a form. The key is to use EL to indicate which iterator should be refreshed.

How-to populate an af:selectOneChoice component from a different Data Control than the one used to update the model<br/>

This ADF Insider Essentials shows you how to create dynamic lists to populate an af:selectOneChoice component. It also shows how to populate the list from a different Data Control than the Data Control used to build the editable table or form that host the select one choice component.



<br/>
<br/>
<strong>ADF Controller</strong>
<br/>
<br/>



Passing Data Within A Task Flow<br/>
In a task flow you may want to reference data or values set up in another activity of the task flow. This ADF Insider Essentials shows you how you can pass data in a task flow.



<br/>
<br/>
<strong>ADF Faces</strong>
<br/>
<br/>


Building UI Layout using the ADF Faces Switcher Component<br/>
Sometimes you might want to dynamically switch an area of your page to show a different UI component. This demonstration shows how you can use af:switcher to dynamically change the UI layout.

Storing Different Values to those Displayed Using a selectOneRadio<br/>
For a binary value, such as OrderFilled (Y or N) you may have the use case of storing a Y and N in the database but display different values (e.g. Yes and No, or Filled & Not Filled) to the user. This demonstration shows how you can achieve this with a selectOneRadio component.


Controlling Using Input in Table Column Filters<br/>
This sample shows how to customize the column filter in an af:table component to control user query data input. The sample uses JavaScript to enforce numeric value input on an ID attribute field.


Using af:iterator to Dynamically Render an Image<br/>
How can you dynamically render a UI component a set number of times? For example, for a customer rating (which is a numeric value) you want to "stamp out" a certain number of stars to indicate the rating rather than just display a number (like an Amazon rating). This demonstration shows you how using the af:iterator component.

How-to handle the af:dialog OK and CANCEL buttons<br/>
The ADF Faces dialog component is a child component of the af:popup component and shows a framed content area with optional OK/Cancel and Yes/No buttons. This session explains how to determine which button has been pressed by a user and how to handle the CANCEL event, which, in contrast to the other button options is a pure client side event. It also shows how the CANCEL event can be broadcast to the server for developers to query data or clean up uncommitted changes.

How-to delete node(s) in Oracle ADF bound ADF Faces hierarchical trees<br/>
This sample shows how to determine selected tree nodes and delete them from an action exposed on the tree context menu. The sample shows how the tree is created, the context menu is defined and how to create a managed bean action to handle the tree node(s) delete operation. A topic covered in here too is the confusion that exists about View Accessors and how the tree structure uses internal APIs to access child collections.

Strategy for creating page templates with global button functionality<br/>
Page templates ensure consistent layouts being used throughout an application and, if the template is deployed and reused in an ADF library, across applications. A frequent requirement is to build layouts with default functionality, like global buttons. Global buttons execute application wide functionality, like navigating to a login page or a help page, but may also execute page context specific functionality like creating a new record or deleting a current row. This session shows how the ADF Faces page template can be used to build layout blueprints that contain global buttons for invoking application wide and context specific functionality.


Programmatic Partial Page Refresh<br/>
ADF Faces includes a number of declarative features for controlling the submission and refresh of the ADF Faces pages. This is called Partial Page Refresh (PPR). In this sample you will see how to programmatically control the partial page refresh feature of ADF Faces.

Customizing the ADF Search Form Initial Rendering and Query Behavior<br/>
This ADF Insider essential recording shows how to initially render the ADF search form in advanced mode and how to change the query behavior to case insensitive. As an extra goody, you learn how to use a custom view criteria with ADF table filters.

Undo ADF table selection on validation error<br/>
This ADF Insider essential recording shows what do you do if uncommitted data exists when users change the row currency of a table. If uncommitted data exists, an alert is shown to remind the user to commit the changes before selecting another row in a table. If data change validation succeeds, the system sets the new table row as the current row in the underlying binding layer. Though the example uses data commit as the validation exit criteria, it can be any Java accessible condition that determines whether the row selection needs to be undone or not.


<br/>

### ADF Debugging


How to implement logging in an ADF application<br/>
Yes, the ADF source is available! This ADF Insider Essentials shows you how to add the ADF source to JDeveloper and use it to debug your application, as well as how to implement logging for ADF applications.

Debugging task flows and memory scope<br/>
In this ADF Insider Essentials, you learn how to debug typical errors that occur in ADF applications. Through an example, tips for triaging and debugging an issue using the JDeveloper debugger and the ADF task flow debugger are given.



<br/>

### General ADF Techniques


Deploying reusable components<br/>
To maintain consistency, page templates and other common UI elements should be shared among different applications. Additionally, you can create a reusable data model using ADF Business Components and build user interfaces upon the common data model. In this ADF Insider Essential you will see how you can distribute these reusable components using an ADF Library.
