---
layout: page
title: ADF Binding Internals - Intro & metadata files P1
permalink: /learn-oracle-adf/oracle-adf-insider/adf-bindings-internals-part-1/
---

# ADF Binding Internals - Intro & metadata files P1


<strong>ADF Binding Internals - Intro & metadata files P1</strong>


<div align="center">

<iframe width="640" height="360" src="http://www.youtube.com/embed/iYMQFBg8Q64" frameborder="0" allowfullscreen></iframe>


</div>


<br/>
<br/>

<div align="center">
	<iframe src="http://www.slideshare.net/slideshow/embed_code/10607172" width="962" height="826" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC;border-width:1px 1px 0;margin-bottom:5px" allowfullscreen webkitallowfullscreen mozallowfullscreen> </iframe>

</div>

<br/>
