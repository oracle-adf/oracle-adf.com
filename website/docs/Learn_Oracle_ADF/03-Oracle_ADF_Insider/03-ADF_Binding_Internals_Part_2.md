---
layout: page
title: ADF Binding Internals - Intro & metadata files P2
permalink: /learn-oracle-adf/oracle-adf-insider/adf-bindings-internals-part-2/
---

# ADF Binding Internals


<br/>
<br/>
<strong>Accessing Binding at Runtime P2</strong>
<br/>
<br/>


<div align="center">

<iframe width="640" height="360" src="http://www.youtube.com/embed/j7okkGBjE6w" frameborder="0" allowfullscreen></iframe>

</div>

<br/>
