---
layout: page
title: Using JavaScript in Oracle ADF Faces Application Development
permalink: /learn-oracle-adf/oracle-adf-insider/using-javascript-in-oracle-adf-faces/
---

# Using JavaScript in Oracle ADF Faces Application Development


<div align="center">

<iframe width="640" height="360" src="http://www.youtube.com/embed/9_6K4xJov88" frameborder="0" allowfullscreen></iframe>

</div>
