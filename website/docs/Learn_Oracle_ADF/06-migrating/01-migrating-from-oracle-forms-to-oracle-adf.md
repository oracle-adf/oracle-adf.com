---
layout: page
title:  Migrating from Oracle Forms to Oracle ADF
permalink: /learn-oracle-adf/migrating/migrating-from-oracle-forms-to-oracle-adf/
---

# Successfully move from Oracle Forms to Oracle ADF

<div align="center">
    <iframe width="560" height="315" src="https://www.youtube.com/embed/5gIpKNfnPms" frameborder="0" allowfullscreen></iframe>
</div>

<br/><br/>

### Successfully move from Oracle Forms to Oracle ADF

<div align="center">
    <iframe width="560" height="315" src="https://www.youtube.com/embed/FF62gqPfLNE" frameborder="0" allowfullscreen></iframe>
</div>

<br/><br/>

<div align="center">

    <a href="http://files.javadev.org/adf/migration/ADF_Integration_Webinar.pptx">Presentations</a>

</div>
