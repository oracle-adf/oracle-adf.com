---
layout: page
title:  Scopes in Fusion Web Applications
permalink: /docs/info/scopes-in-fusion-web-applications/
---

## Scopes in Fusion Web Applications

Scope indicates life time of an object. In ADF and JSF it is possible to define backing beans with particular scope.

### Types of Scopes

There are seven types of scopes in a Fusion web application:

Scopes listed below in descending order with first one having longest life time and last one lowest life time.

Application scope -> Session scope ->Page flow scope  -> View scope ->Request scope ->Backing  bean scope

* Application scope: An application scope object is available for the duration of the application and is shared among users. This scope may be used to hold static objects that are the same for all users.

* Session scope: The object is available for the duration of the session, which is user instance-specific. A use case for a session scope bean is a user info bean that stores information about a user, which is read from the database or an LDAP server, to avoid unnecessary queries.

* Page flow scope (Task flow scope): A pageFlow scope exists for each task flow instance and has a lifespan between request and session scope. The lifetime of the scope spans across all pages in a bounded task flow.

* Request scope: The object is available from the time an HTTP request is made until a response is sent back to the client. From another perspective, a request scope starts with a request to be issued from one view to another for navigation cases that don't perform a redirect but a default server-side forward. The scope spans across all non-view activities that follow the view of interest to the next view activity.

* Backing bean scope: The backing bean scope is comparable to the request scope, with the difference in that it exists for a specific client component. In general, all managed beans used in reusable components should be configured to backingBean scope. For example, bounded task flows that are designed to be regions on a page should use the backingBean scope if more than one instance of the task flow is expected to be on a single page.

* View scope (Page Scope): The object is available until the view ID for the current view activity changes. This becomes handy when you use partial page rendering. If you have a dependent list box, you might send a server request to refresh the list box. When a response is returned, the request scope will be gone but the view scope will be still there. Therefore, view scope can be used to store data when partial rendering request comes back. The view scope exists not only for views that are rendered by JSPX pages, but also for views rendered by page fragments, as is the case in task flows that are built to execute in a region. The view scope of the parent page is not accessible from components added to a page fragement in a region, and the view scope of a view in a region is not accessible for the parent page.

* None:  When you create objects (such as a managed bean) that require you to define a scope, you can set the scope to none, meaning that it will not live within any particular scope, but will instead be instantiated each time it is referenced. You should set a bean's scope to none when it is referenced by another bean.

Out of these View scope, Backing bean scope and page flow scope are ADF specific. Rest comes with JSF. This is the reason you must define ADF specific scoped backing beans ether in "adf-config.xml" or task flow. You access objects in those scopes via expression language with scope qualification. For instance, to reference the aaBean managed bean from viewScope scope, your expression would be
#{viewScope .aaBean }.

When working in ADF its always recommended to define backing beans in  ether "adf-config.xml" or task flow files.

Original:  
http://adfscopes.blogspot.ru/2011/02/scopes-in-fusion-web-applications.html
