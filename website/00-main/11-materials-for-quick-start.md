---
layout: page
title: Materials for Quick Start
permalink: /materials-for-quick-start/
---

# Materials for Quick Start

<br/>

<a href="/files/pdf/materials/Oracle_ADF_On_Ramp.pdf">Oracle ADF On-Ramp: Developing Applications With the Core ADF Stack</a><br/>

<a href="/learn-oracle-adf/oracle-11g-adf-university/">Oracle 11g ADF University</a><br/>

<a href="/learn-oracle-adf/oracle-adf-insider/">Oracle ADF Insider</a><br/>

<a href="/docs/materials/code-corner/">ADF Code Corner Articles</a> (Links for discussions and update materials)<br/>
