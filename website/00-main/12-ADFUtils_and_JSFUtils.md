---
layout: page
title: ADF Utils / JSF Utils
permalink: /oracle-adf-jsf-utils/
---

# ADF UTILS

<br/>

We made public repos with <a href="https://bitbucket.org/oracle-adf/adf-utils">Utils</a>.

This project extends ADF/JSF Utils from project FusionOrderDemo_R2_1_revised by Oracle employee.

You can use our codes without any restrictions. And if you can improve it, please send pull request.

Happy coding!
