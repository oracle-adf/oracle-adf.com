---
layout: page
title:  Oracle JDeveloper 12.2.1 Overview
permalink: /overview/
---


# Oracle JDeveloper 12.2.1 Overview

<br/>

<div align="center">
    <iframe width="640" height="480" src="https://www.youtube.com/embed/63rnCGawF9w" frameborder="0" allowfullscreen></iframe>
</div>

<br/>

<h3>Oracle ADF 12.2.1 Overview</h3>

<div align="center">
    <iframe width="640" height="480" src="https://www.youtube.com/embed/5ZaRerw3U28" frameborder="0" allowfullscreen></iframe>
</div>
