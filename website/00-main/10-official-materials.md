---
layout: page
title: Official Materials
permalink: /official-materials/
---

# Official Materials

<h3>Oracle Fusion Middleware 12</h3>

<br/>

<a href="http://docs.oracle.com/middleware/1221/adf/index.html" rel="nofollow">Oracle Fusion Middleware 12c (12.2.1): Oracle Application Development Framework (ADF) - Tasks </a><br/>

<a href="http://docs.oracle.com/cd/E14571_01/web.1111/b31974/toc.htm" rel="nofollow">Fusion Middleware Fusion Developer's Guide for Oracle Application Development Framework</a><br/>

<a href="https://docs.oracle.com/middleware/1221/adf/develop-faces/toc.htm" rel="nofollow">Oracle® Fusion Middleware Developing Web User Interfaces with Oracle ADF Faces</a> (PDF)<br/>

<br/><br/>

<h3><a href="http://docs.oracle.com/cd/E12839_01/nav/dev.htm" rel="nofollow">Oracle Fusion Middleware Online Documentation Library 11g Release 1 (11.1.1.1)</a></h3>

<br/><br/>

<a href="http://docs.oracle.com/cd/E12839_01/web.1111/b31974.pdf" rel="nofollow">Fusion Developer's Guide</a>(PDF)<br/>
<a href="http://docs.oracle.com/cd/E12839_01/web.1111/b31973.pdf" rel="nofollow">Web User Interface Developer's Guide</a>(PDF)<br/>
<a href="http://docs.oracle.com/cd/E12839_01/web.1111/e10139.pdf" rel="nofollow">Desktop Integration Developer's Guide</a>(PDF)<br/>
<a href="http://docs.oracle.com/cd/E12839_01/web.1111/e10140.pdf" rel="nofollow">Mobile Developer's Guide</a>(PDF)<br/>

<br/><br/>

<h3>Other</h3>

<br/><br/>

<a href="http://www.oracle.com/technetwork/developer-tools/adf/learnmore/adffaceslayoutbasics-2046652.pdf" rel="nofollow">ADF Faces Layout Basics</a>(PDF)<br/>
<a href="/files/pdf/groovy/introduction-to-groovy-128837.pdf" rel="nofollow">Introduction to Groovy Support in JDeveloper and Oracle ADF 11g</a>(PDF)<br/>
