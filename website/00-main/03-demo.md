---
layout: page
title:  Oracle ADF Application Examples
permalink: /demo/
---

# Oracle ADF Application Examples

<div align="center">

    <iframe width="640" height="480" src="https://www.youtube.com/embed/79QQbQ-PDkM" frameborder="0" allowfullscreen=""></iframe>

    <br/><br/>

    <iframe width="640" height="480" src="https://www.youtube.com/embed/JFdW4_nne0A" frameborder="0" allowfullscreen></iframe>

</div>


<!--
<br/><br/>

Project looks like this, you can find <a href="https://github.com/oracle-adf/PaaS-SaaS_UIAccelerator_Without_WebService">here</a>
-->

<br/><br/>

<div align="center">

    <iframe width="853" height="480" src="https://www.youtube.com/embed/vvNBUsIJqaM" frameborder="0" allowfullscreen></iframe>

</div>

http://www.boxfusionconsulting.com/boxfusion-blog/whats-new-oracle-sales-cloud-release-9/

<br/><br/>

<div align="center">

<h3>Simplified User Experience Design Patterns eBook</h3>
<br/><br/>

<img src="https://apex.oracle.com/pls/apex/ebo/r/files/static/v18Y/book-cover_mcgraw_hill.png" alt="Simplified User Experience Design Patterns eBook">


<br/><br/>

    <a href="https://apex.oracle.com/pls/apex/f?p=70974:1:114287088062282:::::i" rel="nofollow">Click to register and download your free copy of the eBook</a>


</div>



<br/><br/>
