---
layout: page
title: Oracle ADF OpenSource Projects
permalink: /src/
---

# [Oracle ADF] OpenSource Projects

<br/>

### Oracle Applications Cloud Accelerators - Sample Code

![Oracle Applications Cloud Accelerators](/img/app/Oracle-Applications-Cloud-Accelerators.jpg "Oracle Applications Cloud Accelerators"){: .center-image }


<br/>

<a href="http://www.oracle.com/technetwork/indexes/samplecode/app-cloud-accelerators-2882163.html"><strong>Website</strong></a>


<br/><br/>

<strong>Work Better (No Database connection is needed)</strong><br/>

<a href="http://jdevadf.oracle.com/workbetter/faces/index"><strong>Website</strong></a> || <a href="https://bitbucket.org/oracle-adf/work-better"><strong>src</strong></a>


<br/><br/>

<strong>UIAccelerator (Cool example. You can grab Skin for your applications)(Connection to DataBase not needed)</strong><br/>

<a href="https://www.youtube.com/watch?v=XaV89ssnC9o"><strong>YouTube</strong></a> || <a href="https://bitbucket.org/oracle-adf/paas-saas_uiaccelerator_without_webservice"><strong>src</strong></a>

<br/><br/>

<strong>Apps cloud ui kit (Cool example. You can grab Skin for your applications)(Connection to DataBase not needed)</strong>

<a href="https://bitbucket.org/oracle-adf/apps-cloud-ui-kit"><strong>src</strong></a>

run --> DemoMaster\Web Content\Welcome.jspx

<br/>

<strong>Summit ADF</strong>

<br/>

<a href="https://bitbucket.org/oracle-adf/summit-adf"><strong>src</strong></a>

<br/><br/>

<strong>Sample ADF Application</strong><br/>

<a href="http://docs.oracle.com/middleware/1221/adf/develop/GUID-17129734-FA96-4637-998B-BE8ABC1EAD9D.htm#ADFFD121"><strong>Info</strong></a> || <a href="http://oracle-adf.com/apps/sample-oracle-adf-application/"><strong>Page</strong></a> || <a href="https://bitbucket.org/oracle-adf/fusionorderdemo_r2_1_revised"><strong>src</strong></a>

 
<br/><br/>

<strong>ADF Use Cases</strong>
<br/>
<a href="https://bitbucket.org/oracle-adf/adf-use-cases"><strong>src</strong></a>

<br/><br/>

<strong>ADF EMG Samples - Table of Contents</strong>
<br/>
<a href="https://java.net/projects/smuenchadf/pages/ADFSamples"><strong>src</strong></a>

<br/><br/>

<strong>ADF Samples - ADF Faces (Not on GItHub)</strong>
<br/>

<a href="https://java.net/projects/smuenchadf/pages/ADFSamplesADFFaces#UI_Components"><strong>src</strong></a>

<br/><br/>

<strong>ADF Samples - UI Components (Not on GItHub)</strong>
<br/>
<a href="https://java.net/projects/smuenchadf/pages/ADFSamplesADFFacesUIComponents"><strong>src</strong></a>

<br/><br/>

<strong>ADF Samples - Performance Tuning (Not on GItHub)</strong>
<br/>
<a href="https://java.net/projects/smuenchadf/pages/ADFPerformanceTuning"><strong>src</strong></a>

<br/><br/>

<strong>Timo Hahn</strong>
<br/>
<a href="https://github.com/tompeez?tab=repositories"><strong>src</strong></a>

<br/><br/>

<strong>Andreus Baranovsky APPS</strong>
<br/>
<a href="https://code.google.com/archive/p/jdevsamples/downloads"><strong>code.google</strong></a>

<a href="https://bitbucket.org/oracle-adf/andrejus-baranovskis/src"><strong>src</strong></a>

<br/><br/>

<strong>adf-sample-applications</strong>
<br/>
<a href="https://bitbucket.org/oracle-adf/adf-sample-applications"><strong>src</strong></a>

<br/><br/>

<strong>Dive into ADF</strong>
<br/>

<a href="https://java.net/projects/smuenchadf/sources/samples/show"><strong>src</strong></a> || <a href="https://blogs.oracle.com/smuenchadf/resource/examples"><strong>src</strong></a>

<br/><br/>

<strong>Examples</strong>
<br/>

<a href="https://bitbucket.org/oracle-adf/adf-app-collection"><strong>src</strong></a>

<br/><br/>

<strong>Oracle JDeveloper and ADF Sample Code</strong>
<br/>
<a href="http://www.oracle.com/technetwork/indexes/samplecode/jdeveloper-adf-sample-522118.html"><strong>src</strong></a>

<br/><br/>

<strong>Security for Everyone By Frank Nimphius</strong>
<br/>
<a href="https://bitbucket.org/oracle-adf/oramag-adf-security"><strong>src</strong></a>


<br/><br/>

In addidional:<br/>

<strong>ADF Faces Rich Client Components Demo</strong>
<br/>

<a href="http://jdevadf.oracle.com"><strong>web</strong></a> ||
<a href="http://www.oracle.com/technetwork/testcontent/adf-faces-rc-demo-083799.html"><strong>web</strong></a>

<br/>


<br/><br/>

If you can offer me any additional projects:<br/>
<img src="/img/a3333333mail.gif" alt="Marley" border="0" />
