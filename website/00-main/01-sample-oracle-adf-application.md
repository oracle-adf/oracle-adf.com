---
layout: page
title: Sample ADF Application
permalink: /apps/sample-oracle-adf-application/
---

# Sample ADF Application

<br/>
<br/>

<strong>Fusion Order Demo (FOD) - Sample ADF Application</strong><br/>

<a href="http://www.oracle.com/technetwork/developer-tools/jdev/index-095536.html"><strong>Website</strong></a> || <a href="https://bitbucket.org/oracle-adf/fusionorderdemo_r2_1_revised/src/master/"><strong>src</strong></a>


<br/>

<strong>Oracle Fusion Order Demo Application For JDeveloper 11.1.1.x</strong><br/>

<a href="http://www.oracle.com/technetwork/developer-tools/jdev/learnmore/fod1111-407812.html"><strong>Website</strong></a> || <a href="https://bitbucket.org/oracle-adf/fusionorderdemo_r1ps5/src/master/"><strong>src</strong></a><br/>

<br/>

<strong>Result:</strong>

<br/>

![Sample ADF Application](/img/app/01_Sample_ADF_Application/Sample_ADF_Application_01.png "Sample ADF Application"){: .center-image }


<br/>
<br/>

![Sample ADF Application](/img/app/01_Sample_ADF_Application/Sample_ADF_Application_02.png "Sample ADF Application"){: .center-image }

<br/>
<br/>

![Sample ADF Application](/img/app/01_Sample_ADF_Application/Sample_ADF_Application_03.png "Sample ADF Application"){: .center-image }
